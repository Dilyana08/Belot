package belot;

import java.util.Iterator;
import java.util.LinkedList;

public class Hand {
	private static LinkedList<Card> cardsInHand;
	
	Hand(){
		cardsInHand = new LinkedList<Card>();
	}
	
	Hand(LinkedList<Card> cardsInHand){
		Hand.cardsInHand = cardsInHand;
	}
	
	public static void addCard(Card card){
		assert(cardsInHand.add(card));	
	}
	
	public static void removeCard(Card card){
		assert(cardsInHand.remove(card));
	}
	
	// Has Some mistakes with sort
	
	public static LinkedList<Card> sortHand(LinkedList<Card> cards, FirstCall call){
		switch (call) {
		case CLUBS:
			sortClubs(cards);
			break;
		case DIAMONDS:
			sortDiamonds(cards);
			break;
		case HEARTS:
			sortHearts(cards);
			break;
		case SPADES:
			sortSpades(cards);
			break;
		case NOTRUMPS:
			sortNoTrumps(cards);
			break;
		case ALLTRUMPS:
			sortAllTrumps(cards);
			break;

		default:
			break;
		}
		
		return cards;
	}
	
	private static LinkedList<Card> sortClubs(LinkedList<Card> cards){
		cards.sort(new ComparatorForClubs());
		return cards;
	}
	
	private static LinkedList<Card> sortDiamonds(LinkedList<Card> cards){
		cards.sort(new ComparatorForDiamonds());
		return cards;
	}
	
	private static LinkedList<Card> sortHearts(LinkedList<Card> cards){
		cards.sort(new ComparatorForHearts());
		return cards;
	}
	
	private static LinkedList<Card> sortSpades(LinkedList<Card> cards){
		cards.sort(new ComparatorForSpades());
		return cards;
	}
	
	private static LinkedList<Card> sortNoTrumps(LinkedList<Card> cards){
		cards.sort(new ComparatorForNoTrumps());
		return cards;
	}
	
	private static LinkedList<Card> sortAllTrumps(LinkedList<Card> cards){
		cards.sort(new ComparatorForAllTrumps());
		return cards;
	}
	
//	public static void main(String[] args){
//		Card one = new Card(Ranks.JACK, Pips.HEARTS);
//		Card two = new Card(Ranks.NINE, Pips.HEARTS);
//		Card three = new Card(Ranks.SEVEN, Pips.SPADES);
//		Card four = new Card(Ranks.TEN, Pips.DIAMONDS);
//		Card five = new Card(Ranks.TEN, Pips.SPADES);
//		Card six = new Card(Ranks.EIGHT, Pips.SPADES);
//		Card seven = new Card(Ranks.ACE, Pips.DIAMONDS);
//		Card eight = new Card(Ranks.TEN, Pips.CLUBS);
//		Card nine = new Card(Ranks.QUEEN, Pips.CLUBS);
//		Card ten = new Card(Ranks.KING, Pips.HEARTS);
//		
//		LinkedList<Card> cards = new LinkedList<Card>();
//		cards.add(one);
//		cards.add(two);
//		cards.add(three);
//		cards.add(four);
//		cards.add(five);
//		cards.add(six);
//		cards.add(seven);
//		cards.add(eight);
//		cards.add(nine);
//		cards.add(ten);
//		
//		Iterator<Card> iterator = cards.iterator();
//		while (iterator.hasNext()) {
//			System.out.println(iterator.next().toString());
//		}
//		
//		sortHand(cards, FirstCall.ALLTRUMPS);
//		System.out.println();
//		
//		Iterator<Card> iteratorTwo = cards.iterator() ;
//		while (iteratorTwo.hasNext()) {
//			System.out.println(iteratorTwo.next().toString());
//		}
//	}
}
