package belot;

import java.util.Comparator;

public class ComparatorForNoTrumps implements Comparator<Card> {
	@Override
	public int compare(Card lhs, Card rhs) {
		return compareNoTrumps(lhs, rhs);
	}

	private int compareNoTrumps(Card lhs, Card rhs) {
		final Ranks[] sortedRanks = { Ranks.SEVEN, Ranks.EIGHT, Ranks.NINE, Ranks.JACK, Ranks.QUEEN, Ranks.KING,
				Ranks.TEN, Ranks.ACE };

		int getLhsIndex = -1;
		int getRhsIndex = -1;

		for (int i = 0; i < sortedRanks.length; i++) {
			if (lhs.getRanks() == sortedRanks[i]) {
				getLhsIndex = i;
			}

			if (rhs.getRanks() == sortedRanks[i]) {
				getRhsIndex = i;
			}
		}

		if (lhs.getPips() == rhs.getPips()) {
			if (getLhsIndex > getRhsIndex) {
				return 1;
			} else if (getLhsIndex < getRhsIndex) {
				return -1;
			} else {
				return 0;
			}
		}
		
		if (lhs.getPips() != rhs.getPips()) {
			if (getLhsIndex > getRhsIndex) {
				return -1;
			} else if (getLhsIndex < getRhsIndex) {
				return 0;
			} else {
				return 0;
			}
		}
		return 0;
	}
}
