package belot;

import java.util.Comparator;

public class ComparatorForAllTrumps implements Comparator<Card> {
	@Override
	public int compare(Card lhs, Card rhs) {
		return compareAllTrumps(lhs, rhs);
	}

	private int compareAllTrumps(Card lhs, Card rhs) {
		final Ranks[] sortedRanks = { Ranks.SEVEN, Ranks.EIGHT, Ranks.QUEEN, Ranks.KING, Ranks.TEN, Ranks.ACE,
				Ranks.NINE, Ranks.JACK };

		int getLhsIndex = -1;
		int getRhsIndex = -1;

		for (int i = 0; i < sortedRanks.length; i++) {
			if (lhs.getRanks() == sortedRanks[i]) {
				getLhsIndex = i;
			}

			if (rhs.getRanks() == sortedRanks[i]) {
				getRhsIndex = i;
			}
		}

		if (lhs.getPips() == rhs.getPips()) {
			if (getLhsIndex > getRhsIndex) {
				return 1;
			} else if (getLhsIndex < getRhsIndex) {
				return -1;
			} else {
				return 0;
			}
		}
		
		if (lhs.getPips() != rhs.getPips()) {
			if (getLhsIndex > getRhsIndex) {
				return -1;
			} else if (getLhsIndex < getRhsIndex) {
				return 0;
			} else {
				return 0;
			}
		}
		return 0;
	}
}
