package belot;

import java.util.Set;

public class Deck {
	
	private static Set<Card> cards;
	
	public Deck(){
		final Pips[] pips = Pips.values();
		final Ranks[] ranks = Ranks.values();
		
		for (int i = 0; i < pips.length; i++) {
			for (int j = 0; j < ranks.length; j++) {
				cards.add(new Card(ranks[j], pips[i]));
			}
		}
	}
	
	public static void shuffle(){ 
	}
	
	public static void split(){
		
	}
	
}
