package belot;

import java.util.Comparator;

public class ComparatorForHearts implements Comparator<Card> {
	@Override
	public int compare(Card lhs, Card rhs) {
		return comparePips(lhs, rhs);
	}

	private int comparePips(Card lhs, Card rhs) {
		if (lhs.getPips() == Pips.HEARTS && rhs.getPips() != Pips.HEARTS) {
			return 1;
		}
		if (lhs.getPips() != Pips.HEARTS && rhs.getPips() == Pips.HEARTS) {
			return -1;
		}
		if (lhs.getPips() == Pips.HEARTS && rhs.getPips() == Pips.HEARTS) {
			return compareRanksSamePips(lhs, rhs);
		}
		if (lhs.getPips() != Pips.HEARTS && rhs.getPips() != Pips.HEARTS) {
			return compareRanksDifferentPips(lhs, rhs);
		} else {
			return 0;
		}
	}

	private int compareRanksSamePips(Card lhs, Card rhs) {
		final Ranks[] sortedRanks = { Ranks.SEVEN, Ranks.EIGHT, Ranks.QUEEN, Ranks.KING, Ranks.TEN, Ranks.ACE,
				Ranks.NINE, Ranks.JACK };

		int getLhsIndex = -1;
		int getRhsIndex = -1;

		for (int i = 0; i < sortedRanks.length; i++) {
			if (lhs.getRanks() == sortedRanks[i]) {
				getLhsIndex = i;
			}

			if (rhs.getRanks() == sortedRanks[i]) {
				getRhsIndex = i;
			}
		}

		if (getLhsIndex > getRhsIndex) {
			return 1;
		} else if (getLhsIndex < getRhsIndex) {
			return -1;
		} else {
			return 0;
		}

	}

	private int compareRanksDifferentPips(Card lhs, Card rhs) {
		final Ranks[] sortedRanks = { Ranks.SEVEN, Ranks.EIGHT, Ranks.NINE, Ranks.JACK, Ranks.QUEEN, Ranks.KING,
				Ranks.TEN, Ranks.ACE };

		int getLhsIndex = -1;
		int getRhsIndex = -1;

		for (int i = 0; i < sortedRanks.length; i++) {
			if (lhs.getRanks() == sortedRanks[i]) {
				getLhsIndex = i;
			}

			if (rhs.getRanks() == sortedRanks[i]) {
				getRhsIndex = i;
			}
		}

		if (getLhsIndex > getRhsIndex && lhs.getPips() == rhs.getPips()) {
			return 1;
		} else if (getLhsIndex < getRhsIndex && lhs.getPips() == rhs.getPips()) {
			return -1;
		} else if (getLhsIndex > getRhsIndex && lhs.getPips() != rhs.getPips()) {
			return -1;
		} else if (getLhsIndex < getRhsIndex && lhs.getPips() != rhs.getPips()) {
			return 0;
		}{
			return 0;
		}
	}
}
