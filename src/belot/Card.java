package belot;

public class Card {
	
	private Pips pip;
	private Ranks rank;
	
	public Card(Ranks rank, Pips pip){
		this.rank = rank;
		this.pip = pip;
	}
	
	public Pips getPips(){
		return this.pip;
	}
	
	public Ranks getRanks(){
		return this.rank;
	}
	
	public void setPips(Pips pip){
		this.pip = pip;
	}
	
	public void setRank(Ranks rank){
		this.rank = rank;
	}
	
	@Override
	public String toString(){
		StringBuilder result = new StringBuilder();
		result.append(getRanks()).append(" of ").append(getPips());
		return result.toString();
	}
}
